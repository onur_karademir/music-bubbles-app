colorArray = [
    "#8E44AD ",
    "#F1948A",
    "#1abc9c",
    "#F1C40F",
    "#7FB3D5",
    "#229954",
];
colorArrayTop = [
    "#0049B7 ",
    "#00DDFF",
    "#fff685",
    "#ff3a22",
    "#acb7ae",
    "#F7882F"
]

window.addEventListener('load', function () {
    const pads = document.querySelectorAll('.pads-container div');
    const padsTop = document.querySelectorAll('.pads-top-container div');
    const soundsBottom = document.querySelectorAll('.pads-container .audio');
    const soundsTop = document.querySelectorAll('.pads-top-container .audio')
    const visual = document.querySelector('.visual');
    const visualTop = document.querySelector('.visual-top');
    const h3 = document.querySelector('h3');
    const p = document.querySelector('p');

    pads.forEach (function (pads,index) {
        pads.addEventListener('click', function () {
            this.classList.add('animated','bounce');
            setTimeout(() => {
                this.classList.remove('animated','bounce')
            }, 1000);
            soundsBottom[index].currentTime = 0;
            soundsBottom[index].play();
            createBubbles(index);
            h3.classList.add('animated','bounce');
            p.classList.add('animated','bounce');
            setTimeout(() => {
                h3.classList.remove('animated','bounce')
                p.classList.remove('animated','bounce')
            }, 500);
        });
    });
    padsTop.forEach(function (padsTop,index) {
        padsTop.addEventListener('click', function () {
            this.classList.add('animated','bounce');
            setTimeout(() => {
                this.classList.remove('animated','bounce')
            }, 1000);
            soundsTop[index].currentTime = 0;
            soundsTop[index].play();
            createTopBubbles(index);
            h3.classList.add('animated','bounce');
            p.classList.add('animated','bounce');
            setTimeout(() => {
                h3.classList.remove('animated','bounce')
                p.classList.remove('animated','bounce')
            }, 500);
        });
    })

    function createBubbles(index) {
        const element = document.createElement('div');
        element.setAttribute('class','bubbles');
        element.setAttribute('id',index);
        visual.appendChild(element);
        element.style.backgroundColor = colorArray[index];
        element.style.animation = 'bubblesJump 1.5s ease';
        element.addEventListener('animationend', function () {
            visual.removeChild(this)
        })
    }
    function createTopBubbles(index) {
        const element = document.createElement('div');
        element.setAttribute('class','bubbles');
        element.setAttribute('id',index);
        visualTop.appendChild(element);
        element.style.backgroundColor = colorArrayTop[index];
        element.style.animation = 'bubblesTopFall 2s ease';
        element.addEventListener('animationend', function () {
            visualTop.removeChild(this)
        })
    };

    // function addClass(className) {
    //     p.classList.add(`${className}`);
    //     h3.classList.add(`${className}`)
    // }
});

